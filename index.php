<?php
require_once 'vendor/autoload.php';

use transit_realtime\FeedMessage;


$return_array = array();

switch(@$_GET['request']){
	case 'alert':
		$data = file_get_contents("http://205.143.55.253:8250/alert/Alerts.pb");
		$feed = new FeedMessage();
		$feed->parse($data);
		foreach ($feed->getEntityList() as $entity) {
			$alert = $entity->getAlert();

			$alert_array = array(
				'id' => $entity->id,
				'effect' => $alert->effect,
				'header_text' => $alert->header_text->translation[0]->text,
				'description_text' => $alert->description_text->translation[0]->text,
				'active_period' => array(
					'start' => date("Y-m-d H:i:s", $alert->active_period[0]->start),
					'end' => date("Y-m-d H:i:s", $alert->active_period[0]->end)
				),
				'informed_entity' => array(
					'route_id' => $alert->informed_entity[0]->route_id,
					'stop_id' => $alert->informed_entity[0]->stop_id
				),
			);
			$return_array[] = $alert_array;
		}
		break;
	case 'tripupdate':

		$data = file_get_contents("http://205.143.55.253:8250/tripupdate/TripUpdates.pb");
		$feed = new FeedMessage();
		$feed->parse($data);
		foreach ($feed->getEntityList() as $entity) {
			$trip = $entity->getTripUpdate();

			$trip_array = array(
				'id' => @$entity->id,
				'trip' => array(
					'trip_id' => @$trip->trip->trip_id,
					'start_time' => @$trip->trip->start_time,
					'start_date' => date("Y-m-d", strtotime(@$trip->trip->start_date)),
					'route_id' => @$trip->trip->route_id,
//					'direction_id' => @$trip->trip->direction_id
				),
				'vehicle' => array(
					'id' => @$trip->vehicle->id,
					'label' => @$trip->vehicle->label,
//					'license_plate' => @$trip->vehicle->license_plate
				),
				'timestamp' => @$trip->timestamp,
				'delay' => @$trip->delay,
			);

			$updates = array();
			foreach($trip->stop_time_update as $update){
				if(@$update->departure->time){
					$time = date("Y-m-d H:i:s", @$update->departure->time);
				}else{
					$time = '';
				}
				$updates[] = array(
					'time' => $time,
					'departure' => array(
						'time' => $time,
						'delay' => @$update->departure->delay,
						'uncertainty' => @$update->departure->uncertainty
					),
					'stop_sequence' => @$update->stop_sequence,
					'stop_id' => @$update->stop_id,
				);
			}
			$trip_array['stop_time_update'] = $updates;

			$return_array[] = $trip_array;
		}
		break;
	case 'vehicle':

		$data = file_get_contents("http://205.143.55.253:8250/vehicle/VehiclePositions.pb");
		$feed = new FeedMessage();
		$feed->parse($data);
		foreach ($feed->getEntityList() as $entity) {
			$vehicle = $entity->getVehicle();


			$vehicle_array = array(
				'id' => @$entity->id,
				'trip' => array(
					'trip_id' => @$vehicle->trip->trip_id,
					'start_time' => @$vehicle->trip->start_time,
					'start_date' => date("Y-m-d", strtotime(@$vehicle->trip->start_date)),
					'route_id' => @$vehicle->trip->route_id,
//					'direction_id' => @$vehicle->trip->direction_id
//					'schedule_relationship' => @$vehicle->trip->schedule_relationship
				),
				'position' => array(
					'latitude' => @$vehicle->position->latitude,
					'longitude' => @$vehicle->position->longitude,
					'bearing' => @$vehicle->position->bearing,
					'odometer' => @$vehicle->position->odometer,
					'speed' => @$vehicle->position->speed,
				),
				'vehicle' => array(
					'id' => @$vehicle->vehicle->id,
					'label' => @$vehicle->vehicle->label,
//					'license_plate' => @$trip->vehicle->license_plate
				),
				'stop_id' => @$vehicle->stop_id,
				'timestamp' => @$vehicle->timestamp,
				'congestion_level' => @$vehicle->congestion_level,
				'current_status' => @$vehicle->current_status,
				'occupancy_status' => @$vehicle->occupancy_status,
//				'delay' => @$entity->delay,
			);

			$return_array[] = $vehicle_array;
		}
		break;
}


header('Content-Type: application/json');
echo json_encode($return_array);
